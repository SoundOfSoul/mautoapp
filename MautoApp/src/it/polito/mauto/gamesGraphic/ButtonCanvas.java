package it.polito.mauto.gamesGraphic;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

public class ButtonCanvas {

	private int x,y;
	private int height, width;
	private String text;
	private float size;
	
	public ButtonCanvas(int x, int y, int height, int width, String text, float size) {
		this.x=x;
		this.y=y;
		this.height=height;
		this.width=width;
		this.text=text;
		this.size=size;
	}
	
	/**
	 * Draw a button with a text inside
	 * @param c
	 * @param pButton
	 * @param pText
	 * @return Canvas
	 */
	public Canvas drawButton(Canvas c, Paint pButton, Paint pText) {
		
		RectF rf = new RectF(new Rect(x, y, x+width, y+height));
		c.drawRoundRect(rf, 10, 10, pButton);
		
		Subtexter sb = new Subtexter();
		ArrayList<String> content = sb.subtexter(text, width, pButton);
		
		float textLenght = 0;
		float xCord = 0;
		float yCord = 0;
		
		for(int i=0; i<content.size(); i++) {
			textLenght = pButton.measureText(content.get(i));
			xCord = x+width/2-textLenght/2;
			yCord = ((i+1)*size)+y+height/2-size/2;
			
			c.drawText(content.get(i), xCord, yCord, pText);
		}
		return c;
	}

	/**
	 * Check if a button is clicked
	 * @param x2
	 * @param y2
	 * @return boolean
	 */
	public boolean checkClick(float x2, float y2) {
		if(x2>x && x2<x+width && y2>y && y2<y+height) {
			return true;
		} else {
			return false;
		}
	}
}
