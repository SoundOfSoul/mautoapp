package it.polito.mauto.gamesGraphic;

import java.util.ArrayList;

import android.graphics.Paint;

public class Subtexter {
	
	public Subtexter() {
		// Do Nothing
	}
	
	/**
	 * Suddivide una stringa in piu stringhe in base alla dimensione della vista contenitore
	 * @param text
	 * @param width
	 * @return ArrayList<String>
	 */
	public ArrayList<String> subtexter(String text, int viewWidth, Paint p) {
		ArrayList<String> subtext = new ArrayList<String>();
		
		char[] c = text.toCharArray();
		String temp = "";
		
		for(int i=0; i<c.length; i++){
			temp = temp+c[i];
			if(p.measureText(temp) >= viewWidth){
				subtext.add(temp.trim());
				temp = "";
			}
		}
		subtext.add(temp.trim());
		
		return subtext;
	}

}
