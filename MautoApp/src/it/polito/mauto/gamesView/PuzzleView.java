package it.polito.mauto.gamesView;

import it.polito.mauto.Activity.R;
import it.polito.mauto.Activity.TechnicalActivity;
import it.polito.mauto.Activity.R.id;
import it.polito.mauto.Activity.R.layout;
import it.polito.mauto.Activity.R.menu;
import it.polito.mauto.gamesModel.ButtonMatch;
import it.polito.mauto.gamesModel.Match;
import it.polito.mauto.gamesModel.Puzzle;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RelativeLayout.LayoutParams;

public class PuzzleView extends RelativeLayout implements OnTouchListener{
	private TextView[] txt;
	private ButtonMatch[] bm;
	private View selected_item;
	private LayoutParams imageParams;
	private Puzzle matchImage;
	private int[][] idAssociated;
	private int countAssociation, offset_x = 0, offset_y = 0, crashX, crashY;
	private boolean touchFlag = false;
	private boolean[] buttonChecked = {false, false, false, false};

	
	public PuzzleView(Context context, Puzzle p) {
		super(context);
        this.matchImage = p;
        
        // Faccio l'inflate della view e prendo il contenitore di tutto
		View.inflate(context, R.layout.layout_puzzle, this);
		ViewGroup container = (ViewGroup) findViewById(R.id.container);
		container.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				if (touchFlag == true) {
					switch (event.getActionMasked()) {
					
					case MotionEvent.ACTION_DOWN:
						for(int i=0; i<bm.length; i++) {
							bm[i] = new ButtonMatch(txt[i+4]);
						}
						break;
						
					case MotionEvent.ACTION_MOVE:
						crashX = (int) event.getX();
						crashY = (int) event.getY();

						int x = (int) event.getX() - offset_x;
						int y = (int) event.getY() - offset_y;
						
						RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
								new ViewGroup.MarginLayoutParams(
										RelativeLayout.LayoutParams.MATCH_PARENT,
										RelativeLayout.LayoutParams.WRAP_CONTENT
								)
						);
						
						lp.setMargins(x, y, 0, 0);
						
						// Drop Image Here
						selected_item.setLayoutParams(lp);
						break;
						
					case MotionEvent.ACTION_UP:
						touchFlag = false;
						
						/*for(int i=0; i<4; i++) {
							if (bm[i].checkClick(crashX, crashY) && !buttonChecked[i]) {
								bm[i].setParameters(selected_item);
								buttonChecked[i] = true;
								selected_item.setVisibility(View.INVISIBLE);
								idAssociated[countAssociation][0] = matchImage.getIdHold(bm[i].getText());
								TextView item = (TextView) selected_item;
								idAssociated[countAssociation][1] = matchImage.getIdDrop(""+item.getText());
								countAssociation++;
								
								if(countAssociation == 4) {
									boolean correct = true;

									// Controllo se tutte le associazioni sono corrette
									for(int j=0; j<4; j++) {
										if(idAssociated[j][0] != idAssociated[j][1]) {
											correct = false;
											break;
										}
									}
									
									// Preparo l'intent
									Intent matchIntent = new Intent(getContext(), TechnicalActivity.class);
									matchIntent.putExtra("gameID", matchImage.getGameID());
									matchIntent.putExtra("gamePerformed", true);
									// Preparo un messaggio di feedback per l'utente
									Toast message = null;
									
									// Mi comporto in maniera diversa in base alla correttezza del gioco
									if(correct) {
										message = Toast.makeText(getContext(),"Hai vinto!",Toast.LENGTH_SHORT);     			
					        			matchIntent.putExtra("gameWellDone", true);
									} else {
										message = Toast.makeText(getContext(),"Hai sbagliato!",Toast.LENGTH_SHORT);
					        			matchIntent.putExtra("gameWellDone", false);
									}
									
									// Lancio l'intent e mostro il messaggio
									message.show();
									getContext().startActivity(matchIntent);
									((Activity) getContext()).finish();
								}
							} else {
								selected_item.setLayoutParams(imageParams);
							}
						}*/
						
						break;
					default:
						break;
					}
				}
				return true;
			}
		});
	}
	
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getActionMasked()) {
		
		// Pressione del dito sullo schermo touch
		case MotionEvent.ACTION_DOWN:
			touchFlag = true;
			offset_x = (int) event.getX();
			offset_y = (int) event.getY();
			selected_item = v;
			imageParams = (LayoutParams) v.getLayoutParams();
			break;
		
		// Distacco del dito dallo schermo
		case MotionEvent.ACTION_UP:
			selected_item = null;
			touchFlag = false;
			break;
		default:
			break;
		}
		return false;
	}
}
