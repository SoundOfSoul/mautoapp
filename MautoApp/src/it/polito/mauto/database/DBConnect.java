package it.polito.mauto.database;

import it.polito.mauto.customException.NoCarsInCategoryException;
import it.polito.mauto.customException.NoCorrectAnswerSetException;
import it.polito.mauto.customException.NoElementsInDBException;
import it.polito.mauto.gamesModel.Car;
import it.polito.mauto.gamesModel.Game;
import it.polito.mauto.gamesModel.Match;
import it.polito.mauto.gamesModel.MatchAnswer;
import it.polito.mauto.gamesModel.Quiz;
import it.polito.mauto.gamesModel.QuizAnswer;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Context;
import android.util.Log;

public class DBConnect {
	
	ArrayList<Car> carsList;
	DocumentBuilderFactory factory;
	DocumentBuilder parser;
	Document document;
	Element element;
	NodeList list;
	Node node;
	
	
	/**
	 * Dato il nome di un nodo ne restituisco il valore
	 * @param tag
	 * @return String
	 */
	private String getValue(String tag) {
		NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
		Node node = (Node) nodes.item(0);
		return node.getNodeValue();
	}
	
	/**
	 * Dato il nome di un nodo ne restituisco un array di valori
	 * @param tag
	 * @return ArrayList<String>
	 */
	private ArrayList<String> getValues(String tag) {
		NodeList nodes = element.getElementsByTagName(tag);
		ArrayList<String> values = new ArrayList<String>();

		for(int i=0; i<nodes.getLength(); i++) {
			node = nodes.item(i).getChildNodes().item(0);
			values.add(node.getNodeValue());
		}
		return values;
	}
	
	/**
	 * 
	 * @return ArrayList<QuizAnswer>
	 * @throws NoCorrectAnswerSetException
	 */
	private ArrayList<QuizAnswer> getQuizAnswers() throws NoCorrectAnswerSetException {
		// Creo l'ArrayList che conterra le risposte del quiz 
		ArrayList<QuizAnswer> answerList = new ArrayList<QuizAnswer>();
		// Prendo tutti i tag "risposta" del quiz
		NodeList nodes = element.getElementsByTagName("risposta");
		
		Element elm;
		Node node;
		
		// Ciclo tutti i nodi "risposta"
		for(int i=0; i<nodes.getLength(); i++) {
			// Prendo il nodo
			node = nodes.item(i);
			// Guardo se ha degli attributi
			if(node.hasAttributes()) {
				// Creo un element per leggere gli attributi
				elm = (Element) node;
				// Prendo il nodo all'interno del nodo risposta (si lo so � un casino)
				node = node.getChildNodes().item(0);
				try {
					// Setto la risposta
					answerList.add(new QuizAnswer(node.getNodeValue(), Boolean.parseBoolean(elm.getAttribute("corretta"))));
				} catch (Exception e) {
					throw new NoCorrectAnswerSetException("Errore nel settare la risposta corretta - DBConnet.class - getQuizAnswers");
				}
			}
		}
		return answerList;
	}
	
	/**
	 * Load all cars from DB to an ArrayList<Car>
	 * @param is
	 * @return ArrayList<Car>
	 * @throws NoElementsInDBException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public ArrayList<Car> loadCars(Context context) throws NoElementsInDBException, ParserConfigurationException, SAXException, IOException {
		
		InputStream is = context.getAssets().open("DBautomobili.xml");
		
		carsList = new ArrayList<Car>();
		
		// Preparo tutti gli oggetti necessari
		factory = DocumentBuilderFactory.newInstance();
		parser = factory.newDocumentBuilder();
		document = parser.parse(is);
		list = document.getElementsByTagName("automobile");
		
		if(list.getLength()>0) {
			
			for(int i=0; i<list.getLength(); i++) {
				node = list.item(i);
				
				if(node.getNodeType()==Node.ELEMENT_NODE) {
					// Prendo il primo elemento di tipo "automobile" del DOM
					element = (Element) node;
					
					// Aggiungo all'array_list un nuovo oggetto "Car"
					carsList.add(
						new Car(
							this.getValue("id"),
							Integer.parseInt(this.getValue("anno")), 
							this.getValue("modello"),
							this.getValue("casa"),
							this.getValue("descrizione"),
							this.getValue("categoria"),
							this.getValue("posizione"),
							this.getValue("path")
						)
					);
				}
			}
			return carsList;
		} else {
			throw new NoElementsInDBException();
		}
	}
	
	
	/**
	 * Load games of a specified category
	 * @param routeCategory
	 * @return ArrayList<Game>
	 * @throws NoCarsInCategoryException
	 * @throws NoElementsInDBException
	 * @throws IOException 
	 * @throws SAXException 
	 */
	public ArrayList<Game> loadGames(Context context, String routeCategory) throws NoCarsInCategoryException, NoElementsInDBException, SAXException, IOException {
		
		InputStream is = context.getAssets().open("DBgiochi.xml");
		
		if(carsList!=null && carsList.size()>0) {
			ArrayList<Game> gamesList = new ArrayList<Game>();

			// Preparo tutti gli oggetti necessari
			document = parser.parse(is);
			list = document.getElementsByTagName("gioco");
			
			if(list.getLength()>0) {
				
				for(int i=0; i<list.getLength(); i++) {
					node = list.item(i);
					
					if(node.getNodeType()==Node.ELEMENT_NODE) {
						// Prendo il primo elemento di tipo "gioco" del DOM
						element = (Element) node;
						
						if(element.getAttribute("category").compareTo(routeCategory)==0) {						
							// Aggiungo all'array_list un nuovo oggetto "Game"
							gamesList.add(
								new Game(
									this.getValue("carID"),
									this.getValue("gameID"),
									this.getValue("nome"),
									routeCategory,
									this.getValue("tipo")
								)
							);
						}
					}
				}
				return gamesList;
			} else {
				throw new NoElementsInDBException("There aren't games in DB from the category: "+routeCategory);
			}
		} else {
			throw new NoElementsInDBException("The cars were not loaded from XML file to ArrayList yet");
		}
		
	}
	
	
	public Quiz loadQuiz(Context context, Game g) throws IOException, SAXException, NoCorrectAnswerSetException, NoElementsInDBException {
		
		InputStream is = context.getAssets().open("DBgiochi.xml");
		
		// Preparo tutti gli oggetti necessari
		document = parser.parse(is);
		list = document.getElementsByTagName("quiz");
		
		if(list.getLength()>0) {
			
			for(int i=0; i<list.getLength(); i++) {
				node = list.item(i);
				
				if(node.getNodeType()==Node.ELEMENT_NODE) {
					// Prendo il primo elemento di tipo "gioco" del DOM
					element = (Element) node;
					
					if(element.getAttribute("gameID").compareTo(g.getGameID())==0) {						
						return new Quiz(
								g.getCarID(), 
								g.getGameID(), 
								g.getName(), 
								g.getCategory(), 
								g.getGameType(), 
								this.getValue("domanda"),
								this.getQuizAnswers());
					}
				}
			}
			return null;
		} else {
			throw new NoElementsInDBException("There aren't quiz for the game: "+g.getGameID()+", car: "+g.getCarID());
		}
	}
	
	public Match loadMatch(Context context, Game g) throws IOException, SAXException, NoCorrectAnswerSetException, NoElementsInDBException {
		
		InputStream is = context.getAssets().open("DBgiochi.xml");
		
		// Preparo tutti gli oggetti necessari
		document = parser.parse(is);
		list = document.getElementsByTagName("match");
		
		if(list.getLength()>0) {
			
			for(int i=0; i<list.getLength(); i++) {
				node = list.item(i);
				
				if(node.getNodeType()==Node.ELEMENT_NODE) {
					// Prendo il primo elemento di tipo "gioco" del DOM
					element = (Element) node;
					
					if(element.getAttribute("gameID").compareTo(g.getGameID())==0) {						
						return new Match(
								g.getCarID(), 
								g.getGameID(), 
								g.getName(), 
								g.getCategory(), 
								g.getGameType(), 
								this.getMatch("drop"),
								this.getMatch("hold"));
					}
				}
			}
			return null;
		} else {
			throw new NoElementsInDBException("There aren't quiz for the game: "+g.getGameID()+", car: "+g.getCarID());
		}
	}
	
	private ArrayList<MatchAnswer> getMatch(String tag) throws NoCorrectAnswerSetException {
		// Creo l'ArrayList che conterra le risposte del quiz 
		ArrayList<MatchAnswer> matchList = new ArrayList<MatchAnswer>();
		// Prendo tutti i tag "risposta" del quiz
		NodeList nodes = element.getElementsByTagName(tag);
		
		Element elm;
		Node node;
		
		// Ciclo tutti i nodi tag
		for(int i=0; i<nodes.getLength(); i++) {
			// Prendo il nodo
			node = nodes.item(i);
			// Guardo se ha degli attributi
			if(node.hasAttributes()) {
				// Creo un element per leggere gli attributi
				elm = (Element) node;
				// Prendo il nodo all'interno del nodo tag (si lo so � un casino)
				node = node.getChildNodes().item(0);
				try {
					// Setto la risposta
					matchList.add(new MatchAnswer(Integer.parseInt(elm.getAttribute("id")),node.getNodeValue()));
				} catch (Exception e) {
					throw new NoCorrectAnswerSetException("Errore nel settare la risposta corretta - DBConnet.class - getQuizAnswers");
				}
			}
		}
		return matchList;
	}
	
}
