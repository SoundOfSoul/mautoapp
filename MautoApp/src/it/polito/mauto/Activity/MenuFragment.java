package it.polito.mauto.Activity;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class MenuFragment extends Fragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	   
		View view = inflater.inflate(R.layout.menu_fragment, container, false);
	
	    Button btn_menu_route = (Button) view.findViewById(R.id.layout_menu_btn_route);
	    Button btn_menu_qr = (Button) view.findViewById(R.id.layout_menu_btn_qrcode);
	    
	    setButton(btn_menu_route, RouteActivity.class);
	    setButton(btn_menu_qr, QRScannerActivity.class);
	    
	    return view;
	}
	
	
	private void setButton(Button btn, final Class<?> c) {
		
		btn.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {        		
        		Intent intent = new Intent(getActivity().getApplicationContext(), c);
				startActivity(intent);
				getActivity().finish();
			}
		});
	}
}
