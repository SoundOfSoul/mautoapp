package it.polito.mauto.Activity;

import it.polito.mauto.Activity.R;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	private Route route;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Inizializzo le variabili globali
		this.initApplication();
		
		// Prendo i quattro bottoni di selezione del percorso
		Button race = (Button) findViewById(R.id.layout_route_btn_race);
		Button tech = (Button) findViewById(R.id.layout_route_btn_tech);
		Button famous = (Button) findViewById(R.id.layout_route_btn_famous);
		Button history = (Button) findViewById(R.id.layout_route_btn_history);
		
		this.setButton(race, "Gare e sfide");
		this.setButton(tech, "Tecnologia e design");
		this.setButton(famous, "Personaggi famosi e lusso");
		this.setButton(history, "Primati e storia");
	}
	
	
	private void setButton(Button btn, final String routeCategory) {
		
		btn.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        		try {
					route.startRoute(routeCategory);
					
					// Quando clicco sul bottone mi lancia l'activity route passandogli la categoria del percorso
	        		Intent intent = new Intent(MainActivity.this, RouteActivity.class);
					startActivity(intent);
					finish();
					
				} catch (Exception e) {
					Log.d("MainActivity", e.toString());
					throw new RuntimeException(e);
				}
			}
		});
	}
	
	
	private void initApplication() {
		
		route = (Route) getApplicationContext();
		
		try {
			// Carico il database
			route.loadDB();
			
		} catch (Exception e) {
			Log.d("MainActivity", e.toString());
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_struttura, menu);
		return true;
	}
	
	@Override
	public void onBackPressed() {
		AlertDialog dialog = new AlertDialog.Builder(this).create();
		setExitDialog(dialog).show();
	}
	
	
	private AlertDialog setExitDialog(AlertDialog dialog) {
		dialog.setTitle("Conferma uscita");
	    dialog.setMessage("Vuoi davvero uscire dall'applicazione?");
	    
	    // Add the buttons
		dialog.setButton(DialogInterface.BUTTON_POSITIVE, "SI", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				finish();
			}
		});
		dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		return dialog;
	}
	
}
