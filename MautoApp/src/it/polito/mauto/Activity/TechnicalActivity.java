package it.polito.mauto.Activity;

import it.polito.mauto.gamesModel.Car;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TechnicalActivity extends FragmentActivity {
	
	private Route route;
	private Car carToShow;
	private RelativeLayout viewContainer;
	private boolean gamePerformed = false,
					gameWellDone = false;
	private String gameID = null,
				   carID = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_route);
		
		// Inserisco la vista della scheda tecnica all'interno container
		viewContainer = (RelativeLayout) findViewById(R.id.viewContainer);
		getLayoutInflater().inflate(R.layout.layout_technical, viewContainer);
		
		// Prendo l'application (pu� esistere una sola application per ogni app)
		route = (Route) getApplicationContext();
		
		// Controllo gli extras
		this.checkExstras();
		// Salvo le informazione del gioco terminato
		this.saveGameInfo();
		// Carico la vettura da mostrare in scheda tecnica
		this.loadCarToShow();
		// Setto tutti gli elementi grafici
		this.setViewElements();
	}
	
	
	private void setViewElements() {
		// Sistemo tutti gli elementi della vista
		if(carToShow != null) {
			TextView txt_technical_carModel = (TextView) findViewById(R.id.txt_technical_carModel);
			TextView txt_technical_carMaker = (TextView) findViewById(R.id.txt_technical_carMaker);
			TextView txt_technical_carYear = (TextView) findViewById(R.id.txt_technical_carYear);
			
			// Setto gli elementi con le informazioni della vettura
			txt_technical_carModel.setText(carToShow.getModel());
			txt_technical_carMaker.setText(carToShow.getCarmaker());
			txt_technical_carYear.setText(""+carToShow.getYear());
		}
		
		// Setto il bottone nextStep
		Button btn_technical_nextStep = (Button) findViewById(R.id.btn_technical_nextStep);
		btn_technical_nextStep.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent nextStep = new Intent(TechnicalActivity.this, RouteActivity.class);
				startActivity(nextStep);
				finish();
			}
		});
	}

	
	private void checkExstras() {
		// Controllo se vi sono extras
		Bundle extras = getIntent().getExtras();
		
		if (extras != null) {
			// Scheda tecnica dopo un gioco
			gamePerformed 	= extras.getBoolean("gamePerformed");
			gameWellDone 	= extras.getBoolean("gameWellDone");
			gameID			= extras.getString("gameID");
			// Solo scheda tecnica
			carID 			= extras.getString("carID");
		}
	}
	
	
	private void loadCarToShow() {
		if(carID != null) {
			try {
				carToShow = route.getCarByID(carID);
			} catch (Exception e) {
				Log.w("TechnicalActivity", e.toString());
				throw new RuntimeException(e);
			}
		}
	}
	
	
	private void saveGameInfo() {
		if(gamePerformed) {
			// Setto il gioco come performed
			route.setGamePerformed(gameID);
			
			if(gameWellDone) {
				// Setto il gioco come WellDone (vinto)
				route.setGameWellDone(gameID);
			}
		}
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_technical, menu);
		return true;
	}

}
