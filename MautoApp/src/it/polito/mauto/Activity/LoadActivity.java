package it.polito.mauto.Activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.content.Intent;
import android.widget.ProgressBar;

public class LoadActivity extends Activity {
	private ProgressBar p;
	private CountDownTimer cdt;
	private long pStatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_load);

		p = (ProgressBar)findViewById(R.id.layout_load_progressbar);
		p.setProgress(0);

		final long length = 3000;	//3000 ms = 3 sec
		long period = 100;			//100%
		

		cdt = new CountDownTimer(length, period) { 

	        public void onTick(long millisUntilFinished) {
	        	pStatus = ( length - millisUntilFinished );
	        	int pint = (int)pStatus/20;
	            p.setProgress(0 + pint);	//se fosse "100 - pint" allora farebbe il conto lla rovescia
	            

	            //quando arriva a 100% il caricamento allora cambio schermata
	            if(p.getProgress()==100){
	            	Intent intent = new Intent(LoadActivity.this, MainActivity.class);
					startActivity(intent);
					finish();
	            }
	        }

	        public void onFinish() {
	             // DO something when 2 minutes is up
	        }
	    }.start();
	}
}
