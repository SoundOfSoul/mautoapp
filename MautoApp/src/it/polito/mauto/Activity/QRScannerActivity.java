package it.polito.mauto.Activity;

import net.sourceforge.zbar.Symbol;
import it.polito.mauto.customException.NoCarsInCategoryException;
import it.polito.mauto.zbarscanner.ZBarConstants;
import it.polito.mauto.zbarscanner.ZBarScannerActivity;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class QRScannerActivity extends Activity {
	
	private Route route;
	
	private static final int ZBAR_SCANNER_REQUEST = 0;
    private static final int ZBAR_QR_SCANNER_REQUEST = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Hide the window title.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
		setContentView(R.layout.activity_qrscanner);
		
		route = (Route) getApplicationContext();
		
		this.launchQRScanner(findViewById(R.id.layout));
	}
	
	public void launchScanner(View v) {
        if (isCameraAvailable()) {
            Intent intent = new Intent(this, ZBarScannerActivity.class);
            startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
        } else {
            Toast.makeText(this, "Rear Facing Camera Unavailable", Toast.LENGTH_SHORT).show();
        }
    }

    public void launchQRScanner(View v) {
        if (isCameraAvailable()) {
            Intent intent = new Intent(this, ZBarScannerActivity.class);
            intent.putExtra(ZBarConstants.SCAN_MODES, new int[]{Symbol.QRCODE});
            startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
        } else {
            Toast.makeText(this, "Rear Facing Camera Unavailable", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {    
		String carID = null;
		
	    if (resultCode == RESULT_OK) {
	    	carID = data.getStringExtra(ZBarConstants.SCAN_RESULT).replaceAll("\\s","");
	    	
	    	if(carID!=null && carID.compareTo("")!=0) {
	    		
	    		try {
	    			if(route.isCarInRoute(carID)) {
	    				// Segno lo step come "completato"
	    				route.setStepComplete(carID);
	    				
	    				if(route.haveCarGame(carID)) {
	    					// Lancio il gioco
	    					Intent game = new Intent(QRScannerActivity.this, GameActivity.class);
	    					game.putExtra("carID", carID);
	    					startActivity(game);
	    					finish();
	    				} else {
	    					// Mostro la scheda tecnica
	    					Intent technical = new Intent(QRScannerActivity.this, TechnicalActivity.class);
	    					technical.putExtra("carID", carID);
	    					startActivity(technical);
	    					finish();
	    				}
		    		} else {
		    			// Guardo nel database generale per trovare l'auto e mostro la scheda tecnica
		    			// ...
		    		}
		    	} catch (NoCarsInCategoryException e) {
		    		Log.d("QRScanner", e.getMessage());
		    		throw new RuntimeException(e);
		    	}
		    }
	    }  
	}

}
