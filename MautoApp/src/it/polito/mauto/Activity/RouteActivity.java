package it.polito.mauto.Activity;

import it.polito.mauto.Activity.R;
import it.polito.mauto.gamesModel.Car;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.support.v4.app.FragmentActivity;

public class RouteActivity extends FragmentActivity {
	
	private Route route;
	private RelativeLayout viewContainer;
	private Car carStep;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_route);
		
		// Inserisco la vista del percorso all'interno container
		viewContainer = (RelativeLayout) findViewById(R.id.viewContainer);
		getLayoutInflater().inflate(R.layout.layout_route, viewContainer);
		
		// Prendo l'application (pu� esistere una sola application per ogni app)
		route = (Route) getApplicationContext();
		
		try {
			if(route.thereAreMoreStep()) {
				// Prendo lo step del percorso
				carStep = route.getStep();
				// Setto tutti gli elementi grafici
				this.setViewElements();
				
			} else {
				// Chiudo il percorso
				route.stopRoute();
				
				// Salvo i dati su XML
				// ...
				
				Intent intent = new Intent(RouteActivity.this, MainActivity.class);
				startActivity(intent);
				finish();
			}
			
		} catch (Exception e) {
			Log.w("RouteActivity", e.toString());
			throw new RuntimeException(e);
		}
	}
	
	
	private void setViewElements() {
		// Stampo la posizione dell'auto nel museo
		TextView txt_route_carPosition = (TextView) findViewById(R.id.txt_route_carPosition);
		TextView txt_route_carModel = (TextView) findViewById(R.id.txt_route_carModel);
		
		txt_route_carPosition.setText(carStep.getCarPosition());
		txt_route_carModel.setText(carStep.getModel());
	}
	
	
	private AlertDialog setExitDialog(AlertDialog dialog) {
		dialog.setTitle("Conferma uscita");
	    dialog.setMessage("Vuoi davvero uscire dall'applicazione?");
	    
	    // Add the buttons
		dialog.setButton(DialogInterface.BUTTON_POSITIVE, "SI", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				route.stopRoute();
				dialog.dismiss();
				finish();
			}
		});
		dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		return dialog;
	}
	
	
	@Override
	public void onBackPressed() {
		AlertDialog dialog = new AlertDialog.Builder(this).create();
		setExitDialog(dialog).show();
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_route, menu);
		return true;
	}
}
