package it.polito.mauto.Activity;

import it.polito.mauto.gamesModel.Game;
import it.polito.mauto.gamesModel.Match;
import it.polito.mauto.gamesModel.Quiz;
import it.polito.mauto.gamesView.MatchView;
import it.polito.mauto.gamesView.PowerPlugView;
import it.polito.mauto.gamesView.QuizView;
import it.polito.mauto.Activity.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

public class GameActivity extends Activity {
	
	private Route route;
	private Game gameToPlay = null;
	private String carID = null,
				   gameType = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Hide the window title.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
        // Prendo l'application (pu� esistere una sola application per ogni app)
		route = (Route) getApplicationContext();
		
		// Controllo gli extras
		this.checkExstras();
		// Carica il gioco da lanciare
		this.loadGameToPlay();
		// Controllo se ho gi� giocato a questo gioco
		this.checkIfGameIsAlreadyPlayed();
		// Leggo il tipo di gioco
		gameType = gameToPlay.getGameType();
		
		
		// In base al tipo di gioco determino cosa fare
		if(gameType.compareToIgnoreCase("quiz")==0) {
			// QUIZ GAME
			this.performQuizGame();
		} else if (gameType.compareToIgnoreCase("match")==0) {
			// MATCH GAME
			this.performMatchGame();
		} else if (gameType.compareToIgnoreCase("powerplug")==0) {
			// POWERPLUG GAME
			PowerPlugView ppv = new PowerPlugView(this);
			setContentView(ppv);
		} else {
			Intent leave = new Intent(GameActivity.this, RouteActivity.class);
			startActivity(leave);
			finish();
		}
	}
	
	
	private void performMatchGame() {
		try {
			Match m = route.getMatch(carID);
			MatchView mv = new MatchView(this, m);
			setContentView(mv);
			
		} catch (Exception e) {
			Log.d("GameActivity - MatchGame", e.toString());
			throw new RuntimeException(e);
		}
	}
	
	
	private void performQuizGame() {
		try {
			Quiz q = route.getQuiz(carID);
			QuizView qv = new QuizView(this, q);
			setContentView(qv);
			
		} catch (Exception e) {
			Log.d("GameActivity - QuizGame", e.toString());
			throw new RuntimeException(e);
		}
	}

	
	private void checkIfGameIsAlreadyPlayed() {
		if(gameToPlay.isPerformed()) {
			Intent alreadyPlayed = new Intent(GameActivity.this, TechnicalActivity.class);
			alreadyPlayed.putExtra("carID", carID);
			startActivity(alreadyPlayed);
			finish();
		}
	}
	
	
	private void checkExstras() {
		// Controllo se vi sono extras
		Bundle extras = getIntent().getExtras();
		
		if (extras != null) {
			carID = extras.getString("carID");
		}
	}
	
	
	private void loadGameToPlay() {
		if(carID != null) {
			try {
				gameToPlay = route.getGame(carID);
			} catch (Exception e) {
				Log.w("GameActivity - LoadGameToPaly", e.toString());
				throw new RuntimeException(e);
			}
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_game, menu);
		return true;
	}
	
	@Override
	public void onBackPressed() {
		Intent leave = new Intent(GameActivity.this, RouteActivity.class);
		startActivity(leave);
		finish();		
	}

}
