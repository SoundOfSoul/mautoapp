package it.polito.mauto.Activity;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import it.polito.mauto.customException.NoCarsInCategoryException;
import it.polito.mauto.customException.NoCorrectAnswerSetException;
import it.polito.mauto.customException.NoElementsInDBException;
import it.polito.mauto.customException.NoRouteStartedException;
import it.polito.mauto.customException.RouteAlredyStartedException;
import it.polito.mauto.database.DBConnect;
import it.polito.mauto.gamesModel.Car;
import it.polito.mauto.gamesModel.Game;
import it.polito.mauto.gamesModel.Match;
import it.polito.mauto.gamesModel.Quiz;
import android.app.Application;
import android.util.Log;

public class Route extends Application {

	private ArrayList<Car> carsList;
	private DBConnect dbc = new DBConnect();
	
	private boolean isRunning = false;
	private boolean[] step;
	private ArrayList<Car> carsRoute;
	private ArrayList<Game> gamesRoute;
	
	
	public void loadDB() throws IOException, ParserConfigurationException, SAXException, NoElementsInDBException {
		carsList = dbc.loadCars(this);
		carsRoute = new ArrayList<Car>();
	}
	
	
	public void startRoute(String routeCategory) throws RouteAlredyStartedException, NoElementsInDBException, NoCarsInCategoryException, SAXException, IOException {
		if(!isRunning) {
			isRunning = true;
			this.loadCarsOfRoute(routeCategory);
			step = new boolean[carsRoute.size()];
			this.initStep();
		} else {
			throw new RouteAlredyStartedException();
		}
	}
	
	
	private void loadCarsOfRoute(String routeCategory) throws NoElementsInDBException, NoCarsInCategoryException, SAXException, IOException {
		// Carico tutte le auto del percorso
		if(carsList!=null && carsList.size()>0) {
			for(int i=0; i<carsList.size(); i++) {
				if(carsList.get(i).getCategory().compareTo(routeCategory)==0) {
					carsRoute.add(carsList.get(i));
				}
			}
			if(carsRoute.size()<1) {
				throw new NoCarsInCategoryException();
			}
		} else {
			throw new NoElementsInDBException("You haven't yet load the cars from database");
		}
		
		// Carico i giochi del percorso
		gamesRoute = dbc.loadGames(this, routeCategory);
	}
	
	
	private void initStep() {
		for(int i=0; i<step.length; i++) {
			step[i] = false;
		}
	}
	
	
	public Car getStep() throws NoRouteStartedException {
		Car c = null;
		
		if(isRunning) {
			int i = 0;
			
			for(i=0; i<step.length; i++) {
				if(!step[i]) {
					c = carsRoute.get(i);
					break;
				}
			}
			return c;
		} else {
			throw new NoRouteStartedException();
		}
	}
	
	
	public boolean isCarInRoute(String carID) throws NoCarsInCategoryException {
		if(carsRoute!=null && carsRoute.size()>0) {
			for(int i=0; i<carsRoute.size(); i++) {
				if(carsRoute.get(i).getID().compareTo(carID)==0) {
					return true;
				}
			}
			return false;
		} else {
			throw new NoCarsInCategoryException();
		}
	}

	
	public boolean haveCarGame(String carID) {
		for(int i=0; i<gamesRoute.size(); i++) {
			if(gamesRoute.get(i).getCarID().compareTo(carID)==0) {
				return true;
			}
		}
		return false;
	}
	
	
	public Game getGame(String carID) {
		for(int i=0; i<gamesRoute.size(); i++) {
			if(gamesRoute.get(i).getCarID().compareToIgnoreCase(carID)==0) {
				return gamesRoute.get(i);
			}
		}
		return null;
	}
	
	
	public Quiz getQuiz(String carID) throws IOException, SAXException, NoCorrectAnswerSetException, NoElementsInDBException {
		for(Game g : gamesRoute) {
			if(g.getCarID().compareTo(carID)==0) {
				return dbc.loadQuiz(this, g);
			}
		}
		return null;
	}
	
	
	public Match getMatch(String carID) throws IOException, SAXException, NoCorrectAnswerSetException, NoElementsInDBException {
		for(Game g : gamesRoute) {
			if(g.getCarID().compareTo(carID)==0) {
				return dbc.loadMatch(this, g);
			}
		}
		return null;
	}
	
	
	public void stopRoute() {
		if(isRunning) {
			isRunning = false;
			//salva tutti i dati
		} else {
			throw new RuntimeException();
		}
	}
	
	public boolean thereAreMoreStep() {
		int count = 0;
		for(int i=0; i<step.length; i++) {
			if(step[i]) {
				count++;
			}
		}
		if(count==step.length) return false;
		else return true;
	}

	
	public void setGamePerformed(String gameID) {
		for(int i=0; i<gamesRoute.size(); i++) {
			if(gamesRoute.get(i).getGameID().compareTo(gameID)==0) {
				gamesRoute.get(i).setPerformed();
				break;
			}
		}
	}
	
	
	public void setGameWellDone(String gameID) {
		for(int i=0; i<gamesRoute.size(); i++) {
			if(gamesRoute.get(i).getGameID().compareTo(gameID)==0) {
				gamesRoute.get(i).setWellDone();
				break;
			}
		}
	}
	
	
	public void setStepComplete(String carID) {
		for(int i=0; i<carsRoute.size(); i++) {
			if(carsRoute.get(i).getID().compareTo(carID)==0) {
				step[i] = true;
				break;
			}
		}
	}
	
	
	public Car getCarByID(String carID) {
		for(int i=0; i<carsList.size(); i++) {
			if(carsList.get(i).getID().compareTo(carID)==0) {
				return carsList.get(i);
			}
		}
		return null;
	}
}
