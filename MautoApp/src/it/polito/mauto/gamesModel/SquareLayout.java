package it.polito.mauto.gamesModel;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class SquareLayout extends LinearLayout {
	private int mScale = 2;

	public SquareLayout(Context context, AttributeSet attributes){
		super(context, attributes);
	}
	
	public SquareLayout(Context context) {
		super(context);
	}

//	public void onLayout(boolean changed, int l, int t, int r, int b) {
//		int width = r - l;
//		ViewGroup.LayoutParams params = this.getLayoutParams();
//		params.height = width;
//		this.setLayoutParams(params);
//		this.setMeasuredDimension(width, width);
//		super.onLayout(changed, l, t, r, t + width);
//	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

	    int width = MeasureSpec.getSize(widthMeasureSpec);
	    int height = MeasureSpec.getSize(heightMeasureSpec);

	    if (width > (int)(mScale * height + 0.5)) {
	        width = (int)(mScale * height + 0.5);
	    } else {
	        height = (int)(width / mScale + 0.5);
	    }

	    super.onMeasure(
	            MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
	            MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
	    );
	}
}
