package it.polito.mauto.gamesModel;

public class PuzzleAnswer {
	private int matchID;
	private String name;
	
	public PuzzleAnswer(int matchID, String name) {
		super();
		this.name = name;
		this.matchID = matchID;
	}

	public int getMatchID() {
		return matchID;
	}

	public String getName() {
		return name;
	}
}
