package it.polito.mauto.gamesModel;

import android.R.bool;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class PuzzleMatch {
	
	private int topY, leftX, rightX, bottomY;
	private ImageView imageHold;
	private String associated;
	private boolean checked;
	
	public PuzzleMatch(ImageView imageHold) {
		this.imageHold = imageHold;
		topY = imageHold.getTop();
		leftX = imageHold.getLeft();
		rightX = imageHold.getRight();
		bottomY = imageHold.getBottom();
		checked = false;
	}
	
	
	public boolean checkClick(float crashX, float crashY) {
		if (crashX > leftX && crashX < rightX && crashY > topY && crashY < bottomY) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isChecked(){
		return checked;
	}
	
	public void check(){
		checked = true;
	}
	
}
