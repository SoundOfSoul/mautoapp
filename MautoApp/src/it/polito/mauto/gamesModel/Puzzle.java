package it.polito.mauto.gamesModel;

import java.util.ArrayList;

import android.util.Log;

public class Puzzle extends Game{
	ArrayList<MatchAnswer> dropList;

	public Puzzle(String carID, String gameID, String name, String category, String gameType, 
		ArrayList<MatchAnswer> dropList) {
		super(carID, gameID, name, category, gameType);
		this.dropList = dropList;
	}
	
	
	public MatchAnswer getDrop(int index) {
		return dropList.get(index);
	}
	
	public int getIdDrop(String name){
		for(int i=0;i<9;i++){
			if(dropList.get(i).getName().compareTo(name)==0){
				return dropList.get(i).getMatchID();
			}
		}
		return -1;
	}

}
