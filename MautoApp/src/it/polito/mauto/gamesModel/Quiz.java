package it.polito.mauto.gamesModel;

import java.util.ArrayList;

public class Quiz extends Game {
	
	private String question;
	private ArrayList<QuizAnswer> answerList;
	
	public Quiz(String carID, String gameID, String name, String category, String gameType, String question, ArrayList<QuizAnswer> answerList) {
		super(carID, gameID, name, category, gameType);
		this.question = question;
		this.answerList = answerList;
	}
	
	
	public String getQuestion() {
		return question;
	}
	
	
	public QuizAnswer getAnswer(int index) {
		return answerList.get(index);
	}
	
	public int getNumberOfAnswer() {
		return answerList.size();
	}
}
