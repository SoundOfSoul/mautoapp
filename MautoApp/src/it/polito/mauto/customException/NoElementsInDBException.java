package it.polito.mauto.customException;

public class NoElementsInDBException extends Exception {
	public NoElementsInDBException() {
		super("There aren't elements in the XML DB specified!");
	}
	
	public NoElementsInDBException(String msg) {
		super(msg);
	}
}
