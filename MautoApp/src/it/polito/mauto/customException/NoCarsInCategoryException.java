package it.polito.mauto.customException;

public class NoCarsInCategoryException extends Exception {
	
	public NoCarsInCategoryException() {
		super("There aren't cars in the specified route category!");
	}
	
	public NoCarsInCategoryException(String msg) {
		super(msg);
	}
}