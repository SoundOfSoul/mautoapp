package it.polito.mauto.customException;

public class RouteAlredyStartedException extends Exception {
	public RouteAlredyStartedException() {
		super("The current route is alredy started!");
	}
	
	public RouteAlredyStartedException(String msg) {
		super(msg);
	}
}
