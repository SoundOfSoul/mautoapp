package it.polito.mauto.customException;

public class NoRouteStartedException extends Exception {
	public NoRouteStartedException() {
		super("The corrent route is not started!");
	}
	
	public NoRouteStartedException(String msg) {
		super(msg);
	}
}
