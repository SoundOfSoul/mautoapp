/**-------------LOG-------------
 * VERSIONE: 0.1
 * ULTIMA MODIFICA: Samuele
 * -----------------------------*/

package it.polito.mauto.customException;

public class TooManyCorrectAnswerException extends Exception { 

	public TooManyCorrectAnswerException() {
		super("You can't create more than one correct answer!");
	}
	
	public TooManyCorrectAnswerException(String msg) {
		super(msg);
	}
	
}
