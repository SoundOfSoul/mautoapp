/**-------------LOG-------------
 * VERSIONE: 0.1
 * ULTIMA MODIFICA: Samuele
 * -----------------------------*/

package it.polito.mauto.customException;

public class NoCorrectAnswerSetException extends Exception {
	public NoCorrectAnswerSetException() {
		super("No correct answer has been set!");
	}
	
	public NoCorrectAnswerSetException(String msg) {
		super(msg);
	}
}
