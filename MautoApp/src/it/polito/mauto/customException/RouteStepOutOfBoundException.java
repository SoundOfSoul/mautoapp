package it.polito.mauto.customException;

public class RouteStepOutOfBoundException extends Exception{
	public RouteStepOutOfBoundException() {
		super("There are no more step in the route. Check if you are not exceeding the number of steps!");
	}
	
	public RouteStepOutOfBoundException(String msg) {
		super(msg);
	}
}
